import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import { switchMap } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { UserService } from '../../core/services/user.service';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  cedula;
  user;

  constructor(private userService: UserService, private router: Router,
     private route: ActivatedRoute) { }

  ngOnInit() {
    this.cedula = this.route.snapshot.paramMap.get('cedula');
    this.userService.getUser(this.cedula).subscribe(
      res => {
        this.user = res.body;
      }
    );
  }
  
  update(){
    this.router.navigate(['/dashboard/update']);
  }

  delete() {
    Swal.fire({
      title: 'Estas Segur@ que deseas eliminar este usuario?',
      type: 'warning',
      text: 'no podras recuperar el usuario una vez eliminado.',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!',
      cancelButtonText: 'No, Cancelar',
      reverseButtons: true
    }).then((result) => {
      if(result.value){
        this.userService.deleteUser(this.cedula).subscribe(
          res => {
            if(res.status == 201 || res.status != 200){
              Toast.fire({
                title: "Se ha eliminado el usuario.",
                type: "success",
                onClose: ()=> {
                  this.router.navigate(['/dashboard'])
                }
              });
            }else{
              Toast.fire({
                title: "Hubo un error, intentelo mas tarde",
                type: "error"
              });
            }
          },
          error => {
            Toast.fire({
              title: "Hubo un error, intentelo mas tarde",
              type: "error"
            });
          }
        );
      }
    });
  }
}
