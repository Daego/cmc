export class User{
    cedula:any;
    celular:any;
    direccion:string;
    email:string;
    nombre:string;

    constructor(cedula: number, celular: number, direccion: string,
        email: string, nombre: string){
        this.cedula = cedula;
        this.celular = celular;
        this.direccion = direccion;
        this.email = email;
        this.nombre = nombre;
    }
}