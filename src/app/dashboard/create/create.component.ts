import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../core/services/user.service';
import { Router } from '@angular/router';
import { User } from '../../core/model/user';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  
  submitted:boolean = false;
  createForm = this.fb.group({
    cedula: ['', [Validators.required]],
    celular: [''],
    nombre: [''],
    email: [''],
    direccion: [''],
  }); 

  constructor(private userService: UserService, private fb: FormBuilder, private router:Router) { }

  ngOnInit() {
  }
  
  get cfc() {
    return this.createForm.controls;
  }  
  
  get cfv() {
    return this.createForm.value;
  }
  
  createUser() {
    const user = new User(this.cfv.cedula, this.cfv.celular, this.cfv.direccion,
      this.cfv.email, this.cfv.nombre);
    this.userService.createUser(user).subscribe(
      res => {
        if(res.status == 201 || res.status != 200){
              Toast.fire({
                title: "Se ha creado el usuario",
                type: "success",
                onClose: ()=> {
                  this.router.navigate(['/dashboard'])
                }
              });
            }else{
              Toast.fire({
                title: "Hubo un error, intentelo mas tarde",
                type: "error"
              });
            }
      },
      error => {
        Toast.fire({
          title: "Hubo un error, intentelo mas tarde",
          type: "error"
        });
      }
    );
  }
  
  submit(){
    this.submitted = true;
    console.log("submit")
    if(this.createForm.valid){
      console.log("create")
      this.createUser();            
    }
  }
}
