import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../core/services/user.service';
import { User } from '../../core/model/user';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  submitted:boolean = false;
  cedula;
  user;
  
  updateForm = this.fb.group({
    nombre: [''],    
    celular: [''],
    direccion: [''],
    email: ['', Validators.email],
  });
  
  constructor(private fb: FormBuilder, private userService: UserService, private router: Router,
     private route: ActivatedRoute) { }
  
  
  get ufc(){
    return this.updateForm.controls;
  }
  
  get ufv(){
    return this.updateForm.value;
  }
  
  ngOnInit() {
    this.cedula = this.route.snapshot.paramMap.get('cedula');
    this.userService.getUser(this.cedula).subscribe(
      res => {
        this.user = res.body;
        this.ufc.nombre.setValue(this.user.nombre);
        this.ufc.celular.setValue(this.user.celular);
        this.ufc.direccion.setValue(this.user.direccion);
        this.ufc.email.setValue(this.user.email);
      }
    );
  }
  
  actualizar(){
    const user = new User(this.user.cedula, this.ufv.celular, this.ufv.direccion,
      this.ufv.email, this.ufv.nombre);
    this.userService.updateUser(user).subscribe(
      res => {
        if(res.status == 201 || res.status != 200){
              Toast.fire({
                title: "Se ha actualizado el usuario",
                type: "success",
                onClose: ()=> {
                  this.router.navigate(['/dashboard'])
                }
              })
            }else{
              Toast.fire({
                title: "Hubo un error, intentelo mas tarde",
                type: "error"
              });
            }
      },
      error => {
        Toast.fire({
          title: "Hubo un error, intentelo mas tarde",
          type: "error"
        });
      }
    );
  }
  
  submit()  {
    this.submitted = true;
    if(this.updateForm.valid){
      this.actualizar();
    }
  }
}
