import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { baseUrl} from '../config/config';
import { Auth } from '../model/auth';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
      
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
      })
  }

  private urls: any = {
    auth: baseUrl + 'aut'
  }
  
  constructor(private http: HttpClient) { }
  
  get token() {
    return sessionStorage.token;
  }

  set token(token: any) {
    sessionStorage.token = token;
  }  

  auth(auth: Auth): Observable<any> {
    const params = new HttpParams()
    .set('usuario', auth.usuario)
    .set('password', auth.password)

    return this.http.post<any>(this.urls.auth,  params , this.httpOptions);
  }

  logout() {
    delete this.token;
  }

}
