import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { baseUrl} from '../config/config';
import { AuthService } from './auth.service';



@Injectable({
  providedIn: 'root'
})
export class UserService {

httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.authService.token
    })
}

  private urls: any = {
    allUsers: baseUrl + 'usuario', // get
    createUser: baseUrl +  'usuario', // post
    getUser: baseUrl +  'usuario/', // post usuario/{cedula}
    updateUser: baseUrl + 'usuario/', // put usuario/{cedula}
    deleteUser: baseUrl + 'usuario/' // delete usuario/{cedula}
  }

  constructor(private http: HttpClient, private authService: AuthService) { } 

  allUsers(): Observable<any> {
    return this.http.get<any>(this.urls.allUsers, this.httpOptions);
  }

  createUser(user: User): Observable<any> {
    const params = new HttpParams()
    .set('nombre', user.nombre)
    .set('cedula', user.cedula)
    .set('celular', user.celular)
    .set('direccion', user.direccion)
    .set('email', user.email)
//    return this.http.post<any>(this.urls.createUser, user, this.httpOptions);
    return this.http.post<any>(this.urls.createUser, params, this.httpOptions);
  }

  getUser(cedula): Observable<any> {
    return this.http.get<any>(this.urls.getUser + cedula, this.httpOptions);
  }

  updateUser(user: User): Observable<any> {
    const params = new HttpParams()
    .set('nombre', user.nombre)
    .set('celular', user.celular)
    .set('direccion', user.direccion)
    .set('email', user.email)
    return this.http.put<any>(this.urls.updateUser + user.cedula, params, this.httpOptions);
  }

  deleteUser(cedula: any): Observable<any> {
    return this.http.delete<any>(this.urls.deleteUser + cedula, this.httpOptions);
  }
}
