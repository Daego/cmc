import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { UpdateComponent } from './update/update.component';
import { AllComponent } from './all/all.component';
import { CreateComponent } from './create/create.component';
import { ProfileComponent } from './profile/profile.component';
import { AboutmeComponent } from './aboutme/aboutme.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', component: AllComponent },
      { path: 'update/:cedula', component: UpdateComponent },
      { path: 'all', component: AllComponent },
      { path: 'create', component: CreateComponent },
      { path: 'profile/:cedula', component: ProfileComponent },
      { path: 'aboutme', component: AboutmeComponent },  
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
