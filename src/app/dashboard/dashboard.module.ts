import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { AllComponent } from './all/all.component';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { AboutmeComponent } from './aboutme/aboutme.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [DashboardComponent, AllComponent, CreateComponent, UpdateComponent, AboutmeComponent, ProfileComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CoreModule
  ]
})
export class DashboardModule { }
