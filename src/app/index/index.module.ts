import { NgModule } from '@angular/core';
import { IndexRoutingModule } from './index-routing.module';
import { LoginComponent } from './login/login.component';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CoreModule,
    IndexRoutingModule
  ]
})
export class IndexModule { }
