import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Auth } from '../../core/model/auth';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  @ViewChild("spinner") spinner: ElementRef;
  
  submitted:boolean;
  error:string;

  loginForm = this.fb.group({
    user: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(private authService: AuthService,
    private fb:FormBuilder, private router:Router,
    private renderer: Renderer2, private elmRef: ElementRef) { }

  ngOnInit() {
  }

  get lfc(){
    return this.loginForm.controls;
  }

  get lfv(){
    return this.loginForm.value;
  }

  private login(){
    this.renderer.setStyle(this.spinner.nativeElement, 'display', 'block');
    const auth = new Auth(this.lfv.user, this.lfv.password);
    this.authService.auth(auth).subscribe(
      res => {
        console.log(res);
        if(res.status != 201){
          this.error = "Usuario y/o contraseña incorrecto";
        }else{
          this.authService.token = res.token;
          this.router.navigate(['/dashboard']);
        }
      },
      error => {
        this.renderer.setStyle(this.spinner.nativeElement, 'display', 'none');
        this.error = "Usuario y/o contraseña incorrecto";
      }
      
    );
  }

  submit(){
    this.submitted = true;
    if(this.loginForm.valid){
      this.login();
    }
  }
}
